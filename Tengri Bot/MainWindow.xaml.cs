﻿using AutoIt;
using Memory;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tengri_Bot.Events;
using Tengri_Bot.Variables;

namespace Tengri_Bot
{
    public partial class MainWindow : Window
    {

        public static Mem Memory = new Mem();
        public static WarningWindow WarningWindow = new WarningWindow();
        public static EnemySettings EnemySettings = new EnemySettings();
        static int gameProcId;
        bool isrunning = false;
        string _EnemyList = "";

        public static Stopwatch stopwatch = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();
            HPSlider.Value = Properties.Settings.Default.HPValue;
            MPSlider.Value = Properties.Settings.Default.MPValue;
            HealActiveBox.IsChecked = Properties.Settings.Default.IsHealActive;
            AttackMode.SelectedIndex = Properties.Settings.Default.AttackMode;
            PrayerofGodsPower.IsChecked = Properties.Settings.Default.IsPrayerofGodsPowerActive;
            Wildness.IsChecked = Properties.Settings.Default.IsWildnessActive;
            AC.IsChecked = Properties.Settings.Default.IsACActive;
            HPBuff.IsChecked = Properties.Settings.Default.IsHPBuffActive;
            UseHPPotionBox.IsChecked = Properties.Settings.Default.IsHPPotionActive;
            UseMPPotionBox.IsChecked = Properties.Settings.Default.IsMPPotionActive;
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            gameProcId = Memory.getProcIDFromName("KnightOnLine");
            Memory.OpenProcess(gameProcId);
            if (gameProcId == 0)
            {
                WarningWindow.Warn();
                WarningWindow.Top = this.Top + -68;
                WarningWindow.Left = this.Left + 60;
                WarningWindow.Show();
            }
        }

        private void AddEnemyButton_Click(object sender, RoutedEventArgs e)
        {

            if (AttackMode.SelectedIndex == 3)
            {
                EnemySettings.Show();
            }
            else
            {
                string SelectedEnemy = Memory.readString("KnightOnLine.exe+0x00AFD39C, 0x1FC, 0x1CC, 0x134, 0x8, 0x0").ToString();
                //int Enemy = Memory.readInt("KnightOnLine.exe+0x00B052BC, 0x6CC");
                if (EnemyListBox.Items.IndexOf(SelectedEnemy) == -1)
                {
                    EnemyListBox.Items.Add(SelectedEnemy);
                    _EnemyList += SelectedEnemy + ",";
                    Properties.Settings.Default.EnemyList = _EnemyList;
                }
            }

        }

        private void RemoveEnemyButton_Click(object sender, RoutedEventArgs e)
        {
            EnemyListBox.Items.Remove(EnemyListBox.SelectedItem);
            _EnemyList += EnemyListBox.Items + ",";
            Properties.Settings.Default.EnemyList = _EnemyList;
        }

        private void CloseMainWindow_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsBuffing = false;
            Properties.Settings.Default.IsHealing = false;
            Properties.Settings.Default.EnemyList = "";
            Properties.Settings.Default.Save();
            WarningWindow.Close();
            EnemySettings.Close();
            this.Close();
            

        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (isrunning)
            {
                StartButton.Content = "Start";
                Properties.Settings.Default.IsRunning = false;
                Event.AutoHealWorker.CancelAsync();
                Event.AutoAttackWorker.CancelAsync();
                Event.AutoBufferWorker.CancelAsync();
                Event.AutoPrayerWildnessWorker.CancelAsync();
                isrunning = false;

            }
            else
            {
                StartButton.Content = "Running";
                isrunning = true;
                Properties.Settings.Default.IsRunning = true;
                Event.AutoBuffer();
                Event.AutoPrayerWildness();
                Event.AutoHeal();
                Event.AutoAttack();
                ElapsedTime();


            }

        }

        private void ElapsedTime()
        {
            stopwatch.Restart();
            Application.Current.Dispatcher.Invoke((Action)async delegate
            {
                while (isrunning)
                {
                    if(gameProcId > 0)
                    {
                    //    Process[] processes = Process.GetProcessesByName("knightonline");

                    //   foreach (Process proc in processes)
                    //        SendMessage(proc.MainWindowHandle, WM_KEYDOWN, VK_F5, 0);
                        Elapsed.Content = stopwatch.Elapsed;
                        await Task.Delay(1);
                    } else
                    {
                        stopwatch.Stop();
                        Properties.Settings.Default.IsRunning = false;
                    }
                   
                }
            });
        }

        #region ----------CheckBox Settings------------
        private void HPSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.HPValue = (int)(HPSlider).Value;
        }

        private void MPSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.MPValue = (int)(MPSlider).Value;
        }

        private void HealActiveBox_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsHealActive = true;
        }

        private void HealActiveBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsHealActive = false;
        }

        private void HPTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Properties.Settings.Default.HPPotionKey = HPTextBox.Text;
        }

        private void MPTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Properties.Settings.Default.MPPotionKey = MPTextBox.Text;
        }

        private void AttackSpeed_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AttackSpeed.SelectedIndex == 0)
            {
                Properties.Settings.Default.AttackSpeed = 300;
            }
            if (AttackSpeed.SelectedIndex == 1)
            {
                Properties.Settings.Default.AttackSpeed = 200;
            }
            if (AttackSpeed.SelectedIndex == 2)
            {
                Properties.Settings.Default.AttackSpeed = 100;
            }

        }

        private void AttackMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AttackMode.SelectedIndex == 0)
            {
                Properties.Settings.Default.AttackMode = 0;
            }
            if (AttackMode.SelectedIndex == 1)
            {
                Properties.Settings.Default.AttackMode = 1;
            }
            if (AttackMode.SelectedIndex == 2)
            {
                Properties.Settings.Default.AttackMode = 2;
            }
            if (AttackMode.SelectedIndex == 3)
            {
                AddEnemyButton.Content = "Settings";
                Properties.Settings.Default.AttackMode = 3;
            }
        }

        private void UseHPPotionBox_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsHPPotionActive = true;

        }

        private void UseHPPotionBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsHPPotionActive = false;
        }

        private void UseMPPotionBox_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsMPPotionActive = true;

        }

        private void UseMPPotionBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsMPPotionActive = false;
        }

        private void PrayerofGodsPower_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsPrayerofGodsPowerActive = true;
        }

        private void PrayerofGodsPower_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsPrayerofGodsPowerActive = false;
        }

        private void Wildness_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsWildnessActive = true;
        }

        private void Wildness_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsWildnessActive = false;
        }

        private void AC_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsACActive = true;
        }

        private void AC_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsACActive = false;
        }

        private void HPBuff_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsHPBuffActive = true;
        }

        private void HPBuff_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.IsHPBuffActive = false;
        }
        private void TopMost_Checked(object sender, RoutedEventArgs e)
        {
            Topmost = true;
        }

        private void TopMost_Unchecked(object sender, RoutedEventArgs e)
        {
            Topmost = false;
        }

        #endregion-------CheckBoxSettings-----------
    }
}
