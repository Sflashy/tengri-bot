﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Memory;
namespace Tengri_Bot
{
    /// <summary>
    /// Interaction logic for EnemySettings.xaml
    /// </summary>
    public partial class EnemySettings : Window
    {
        public static Mem Memory = new Mem();
        public static string EnemyList = "";

        static int gameProcId;
        public EnemySettings()
        {
            InitializeComponent();
            gameProcId = Memory.getProcIDFromName("KnightOnLine");
            Memory.OpenProcess(gameProcId);
        }

        private void EnemyAddButton_Click(object sender, RoutedEventArgs e)
        {
            
            int Enemy = Memory.readInt("KnightOnLine.exe+0x00B052BC, 0x6CC");
            if(EnemyListBox.Items.IndexOf(Enemy) == -1)
            {
                EnemyListBox.Items.Add(Enemy);
                EnemyList += Enemy + ",";
                Properties.Settings.Default.EnemyList = EnemyList;
            }
            
        }

        private void CloseEnemyWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void RemoveEnemy_Click(object sender, RoutedEventArgs e)
        {
            EnemyListBox.Items.Remove(EnemyListBox.SelectedItem);
            Properties.Settings.Default.EnemyList = EnemyList;
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
