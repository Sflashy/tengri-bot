﻿#pragma checksum "..\..\EnemySettings.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "3D99B9C2EB9857D31F8A2F6D467F906F30BE1FC7835D16A021E9E2C49AAA82D2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Tengri_Bot;


namespace Tengri_Bot {
    
    
    /// <summary>
    /// EnemySettings
    /// </summary>
    public partial class EnemySettings : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\EnemySettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox EnemyListBox;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\EnemySettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddEnemy;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\EnemySettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CloseEnemyWindow;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\EnemySettings.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RemoveEnemy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Tengri Bot;component/enemysettings.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EnemySettings.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 16 "..\..\EnemySettings.xaml"
            ((Tengri_Bot.EnemySettings)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.EnemyListBox = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.AddEnemy = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\EnemySettings.xaml"
            this.AddEnemy.Click += new System.Windows.RoutedEventHandler(this.EnemyAddButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.CloseEnemyWindow = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\EnemySettings.xaml"
            this.CloseEnemyWindow.Click += new System.Windows.RoutedEventHandler(this.CloseEnemyWindow_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.RemoveEnemy = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\EnemySettings.xaml"
            this.RemoveEnemy.Click += new System.Windows.RoutedEventHandler(this.RemoveEnemy_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

