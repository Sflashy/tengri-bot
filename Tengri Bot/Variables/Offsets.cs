﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tengri_Bot.Variables
{
    class Offsets
    {
        public static string KnightOnLine = "0x00B052BC";
        public static string CHR_NAME = "0x718";
        public static string CHR_HP = "0x74C";
        public static string CHR_MAX_HP = "0x748";
        public static string CHR_MP = "0xC28";
        public static string CHR_MAX_MP = "0xC24";
        public static string CHR_LVL = "0xC20";
        public static string CHR_NOAH = "0xB6C";
        public static string CHR_NEEDEDEXP = "0xB70";
        public static string CHR_CURRENTEXP = "0xC40";
        public static string CHR_NATIONPOINT = "0xC48";
        public static string CHR_STAT_STR = "0xC5C";
        public static string CHR_STAT_HP = "0xC64";
        public static string CHR_STAT_DEX = "0xC6C";
        public static string CHR_STAT_MP = "0xC7C";
        public static string CHR_STAT_INT = "0xC74";
        public static string CHR_STAT_ATTACK_ = "0xC84";
        public static string CHR_STAT_DEFENCE = "0xC8C";
        public static string CHR_STAT_POINT = "0xC1C";
        public static string CHR_COOR_X = "0x128";
        public static string CHR_COOR_Y = "0x120";
        public static string SELECTED_ENTITY_NAME = "0x1FC, 0x1CC, 0x134, 0x8 0x0";
    }
}
