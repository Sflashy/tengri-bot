﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "7C965F491946A7F9111B51B18FBAAF5562AC3267FDB7D5F99F02F6DC82339AD5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Tengri_Bot;


namespace Tengri_Bot {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TengriBot;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border Header;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox TopMost;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CloseMainWindow;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border Heal;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider HPSlider;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider MPSlider;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox UseHPPotionBox;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox HPTextBox;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MPTextBox;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox UseMPPotionBox;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox HealActiveBox;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border Attack;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AttackMode;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AttackSpeed;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox EnemyListBox;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddEnemyButton;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RemoveEnemyButton;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border Support;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox PrayerofGodsPower;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox Wildness;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox AC;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox HPBuff;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button StartButton;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Elapsed;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Tengri Bot;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 16 "..\..\MainWindow.xaml"
            ((Tengri_Bot.MainWindow)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 16 "..\..\MainWindow.xaml"
            ((Tengri_Bot.MainWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.TengriBot = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.Header = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.TopMost = ((System.Windows.Controls.CheckBox)(target));
            
            #line 28 "..\..\MainWindow.xaml"
            this.TopMost.Checked += new System.Windows.RoutedEventHandler(this.TopMost_Checked);
            
            #line default
            #line hidden
            
            #line 28 "..\..\MainWindow.xaml"
            this.TopMost.Unchecked += new System.Windows.RoutedEventHandler(this.TopMost_Unchecked);
            
            #line default
            #line hidden
            return;
            case 5:
            this.CloseMainWindow = ((System.Windows.Controls.Button)(target));
            
            #line 30 "..\..\MainWindow.xaml"
            this.CloseMainWindow.Click += new System.Windows.RoutedEventHandler(this.CloseMainWindow_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Heal = ((System.Windows.Controls.Border)(target));
            return;
            case 7:
            this.HPSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 37 "..\..\MainWindow.xaml"
            this.HPSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.HPSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 8:
            this.MPSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 38 "..\..\MainWindow.xaml"
            this.MPSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.MPSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.UseHPPotionBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 39 "..\..\MainWindow.xaml"
            this.UseHPPotionBox.Checked += new System.Windows.RoutedEventHandler(this.UseHPPotionBox_Checked);
            
            #line default
            #line hidden
            
            #line 39 "..\..\MainWindow.xaml"
            this.UseHPPotionBox.Unchecked += new System.Windows.RoutedEventHandler(this.UseHPPotionBox_Unchecked);
            
            #line default
            #line hidden
            return;
            case 10:
            this.HPTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 40 "..\..\MainWindow.xaml"
            this.HPTextBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.HPTextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.MPTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 41 "..\..\MainWindow.xaml"
            this.MPTextBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.MPTextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.UseMPPotionBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 42 "..\..\MainWindow.xaml"
            this.UseMPPotionBox.Checked += new System.Windows.RoutedEventHandler(this.UseMPPotionBox_Checked);
            
            #line default
            #line hidden
            
            #line 42 "..\..\MainWindow.xaml"
            this.UseMPPotionBox.Unchecked += new System.Windows.RoutedEventHandler(this.UseMPPotionBox_Unchecked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.HealActiveBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 43 "..\..\MainWindow.xaml"
            this.HealActiveBox.Checked += new System.Windows.RoutedEventHandler(this.HealActiveBox_Checked);
            
            #line default
            #line hidden
            
            #line 43 "..\..\MainWindow.xaml"
            this.HealActiveBox.Unchecked += new System.Windows.RoutedEventHandler(this.HealActiveBox_Unchecked);
            
            #line default
            #line hidden
            return;
            case 14:
            this.Attack = ((System.Windows.Controls.Border)(target));
            return;
            case 15:
            this.AttackMode = ((System.Windows.Controls.ComboBox)(target));
            
            #line 50 "..\..\MainWindow.xaml"
            this.AttackMode.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AttackMode_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.AttackSpeed = ((System.Windows.Controls.ComboBox)(target));
            
            #line 60 "..\..\MainWindow.xaml"
            this.AttackSpeed.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AttackSpeed_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.EnemyListBox = ((System.Windows.Controls.ListBox)(target));
            return;
            case 18:
            this.AddEnemyButton = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\MainWindow.xaml"
            this.AddEnemyButton.Click += new System.Windows.RoutedEventHandler(this.AddEnemyButton_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.RemoveEnemyButton = ((System.Windows.Controls.Button)(target));
            
            #line 69 "..\..\MainWindow.xaml"
            this.RemoveEnemyButton.Click += new System.Windows.RoutedEventHandler(this.RemoveEnemyButton_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.Support = ((System.Windows.Controls.Border)(target));
            return;
            case 21:
            this.PrayerofGodsPower = ((System.Windows.Controls.CheckBox)(target));
            
            #line 74 "..\..\MainWindow.xaml"
            this.PrayerofGodsPower.Checked += new System.Windows.RoutedEventHandler(this.PrayerofGodsPower_Checked);
            
            #line default
            #line hidden
            
            #line 74 "..\..\MainWindow.xaml"
            this.PrayerofGodsPower.Unchecked += new System.Windows.RoutedEventHandler(this.PrayerofGodsPower_Unchecked);
            
            #line default
            #line hidden
            return;
            case 22:
            this.Wildness = ((System.Windows.Controls.CheckBox)(target));
            
            #line 75 "..\..\MainWindow.xaml"
            this.Wildness.Checked += new System.Windows.RoutedEventHandler(this.Wildness_Checked);
            
            #line default
            #line hidden
            
            #line 75 "..\..\MainWindow.xaml"
            this.Wildness.Unchecked += new System.Windows.RoutedEventHandler(this.Wildness_Unchecked);
            
            #line default
            #line hidden
            return;
            case 23:
            this.AC = ((System.Windows.Controls.CheckBox)(target));
            
            #line 76 "..\..\MainWindow.xaml"
            this.AC.Checked += new System.Windows.RoutedEventHandler(this.AC_Checked);
            
            #line default
            #line hidden
            
            #line 76 "..\..\MainWindow.xaml"
            this.AC.Unchecked += new System.Windows.RoutedEventHandler(this.AC_Unchecked);
            
            #line default
            #line hidden
            return;
            case 24:
            this.HPBuff = ((System.Windows.Controls.CheckBox)(target));
            
            #line 77 "..\..\MainWindow.xaml"
            this.HPBuff.Checked += new System.Windows.RoutedEventHandler(this.HPBuff_Checked);
            
            #line default
            #line hidden
            
            #line 77 "..\..\MainWindow.xaml"
            this.HPBuff.Unchecked += new System.Windows.RoutedEventHandler(this.HPBuff_Unchecked);
            
            #line default
            #line hidden
            return;
            case 25:
            
            #line 78 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Checked += new System.Windows.RoutedEventHandler(this.AC_Checked);
            
            #line default
            #line hidden
            
            #line 78 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Unchecked += new System.Windows.RoutedEventHandler(this.AC_Unchecked);
            
            #line default
            #line hidden
            return;
            case 26:
            
            #line 79 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Checked += new System.Windows.RoutedEventHandler(this.AC_Checked);
            
            #line default
            #line hidden
            
            #line 79 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Unchecked += new System.Windows.RoutedEventHandler(this.AC_Unchecked);
            
            #line default
            #line hidden
            return;
            case 27:
            this.StartButton = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\MainWindow.xaml"
            this.StartButton.Click += new System.Windows.RoutedEventHandler(this.StartButton_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.Elapsed = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

