﻿using AutoIt;
using Memory;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using Tengri_Bot.Variables;

namespace Tengri_Bot.Events
{
    class Event
    {
        public static BackgroundWorker AutoHealWorker = null;
        public static BackgroundWorker AutoAttackWorker = null;
        public static BackgroundWorker AutoBufferWorker = null;
        public static BackgroundWorker AutoPrayerWildnessWorker = null;
        public static Mem Memory = new Mem();
        static int gameProcId = Memory.getProcIDFromName("KnightOnLine");


        public static void AutoHeal()
        {
            string HPPotionKey = Properties.Settings.Default.HPPotionKey;
            string MPPotionKey = Properties.Settings.Default.MPPotionKey;
            AutoHealWorker = new BackgroundWorker();
            AutoHealWorker.WorkerSupportsCancellation = true;
            AutoHealWorker.DoWork += AutoHealWork;
            
            void AutoHealWork(object sender, DoWorkEventArgs e)
            {
                Memory.OpenProcess(gameProcId);
                while (Properties.Settings.Default.IsRunning)
                {
                    if (AutoHealWorker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (gameProcId > 0)
                    {
                        int MPValue = Properties.Settings.Default.MPValue;
                        int HPValue = Properties.Settings.Default.HPValue;
                        var HP = Memory.read2Byte($"KnightOnLine.exe+{Offsets.KnightOnLine}, {Offsets.CHR_HP}");
                        var MAX_HP = Memory.read2Byte($"KnightOnLine.exe+{Offsets.KnightOnLine}, {Offsets.CHR_MAX_HP}");
                        var MP = Memory.read2Byte($"KnightOnLine.exe+{Offsets.KnightOnLine}, {Offsets.CHR_MP}");
                        var MAX_MP = Memory.read2Byte($"KnightOnLine.exe+{Offsets.KnightOnLine}, {Offsets.CHR_MAX_MP}");

                        if (Properties.Settings.Default.IsHPPotionActive)
                        {

                            if ((float)100 / MAX_HP * HP < HPValue - 20)
                            {
                                if(Properties.Settings.Default.AttackMode == 3)
                                {
                                    AutoItX.WinActivate("Knight OnLine Client");
                                    AutoItX.Send($"{HPPotionKey}");
                                } else
                                {
                                    AutoItX.Send($"{HPPotionKey}");
                                }
                                
                            }
                        }

                        if (Properties.Settings.Default.IsMPPotionActive)
                        {
                            if ((float)100 / MAX_MP * MP < MPValue)
                            {
                                AutoItX.Send($"{MPPotionKey}");
                            }
                        }

                        if (Properties.Settings.Default.IsHealActive)
                        {

                            Properties.Settings.Default.IsHealing = true;
                            if ((float)100 / MAX_HP * HP < HPValue)
                            {
                                AutoItX.Send("{2 DOWN}{2 UP}");
                            }
                            else
                            {
                                Properties.Settings.Default.IsHealing = false;
                            }
                        }

                    }
                    Thread.Sleep(50);
                }
            };
            AutoHealWorker.CancelAsync();
            AutoHealWorker.RunWorkerAsync();
        }

        public static void AutoAttack()
        {
            AutoAttackWorker = new BackgroundWorker();
            AutoAttackWorker.WorkerSupportsCancellation = true;
            AutoAttackWorker.DoWork += AutoAttackWork;

            void AutoAttackWork(object sender, DoWorkEventArgs e)
            {
                Memory.OpenProcess(gameProcId);
                while (Properties.Settings.Default.IsRunning)
                {
                    int AttackSpeed = Properties.Settings.Default.AttackSpeed;
                    int AttackMode = Properties.Settings.Default.AttackMode;
                    string Enemy = Properties.Settings.Default.Enemy;

                    if (gameProcId > 0)
                    {
                        //AutoItX.WinActivate("Knight OnLine Client");
                        if (AutoAttackWorker.CancellationPending == true)
                        {
                            e.Cancel = true;
                            return;
                        }
                        if (Properties.Settings.Default.IsBuffing == false)
                        {

                            if (Properties.Settings.Default.IsHealing == false)
                            {
                                if (AttackMode == 0)
                                {
                                    AutoItX.Send("{Z DOWN}");
                                    Thread.Sleep(50);
                                    AutoItX.Send("{Z UP}");
                                    Thread.Sleep(500);
                                    if (Enemy == Memory.readString($"KnightOnLine.exe+0x00AFD39C, 0x1FC, 0x1CC, 0x134, 0x8, 0x0").ToString())
                                    {
                                        AutoItX.Send("{R DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{R UP}");
                                        Thread.Sleep(AttackSpeed);
                                        AutoItX.Send("{R DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{R UP}");
                                        Thread.Sleep(AttackSpeed);
                                        AutoItX.Send("{R DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{R UP}");
                                        Thread.Sleep(AttackSpeed);
                                    }
                                }
                                if (AttackMode == 1)
                                {
                                    AutoItX.Send("{Z DOWN}");
                                    Thread.Sleep(50);
                                    AutoItX.Send("{Z UP}");
                                    Thread.Sleep(500);
                                    if (Enemy == Memory.readString($"KnightOnLine.exe+0x00AFD39C, 0x1FC, 0x1CC, 0x134, 0x8, 0x0").ToString())
                                    {
                                        AutoItX.Send("{1 DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{1 UP}");
                                        Thread.Sleep(AttackSpeed);
                                        AutoItX.Send("{R DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{R UP}");
                                        Thread.Sleep(AttackSpeed);
                                        AutoItX.Send("{R DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{R UP}");
                                        Thread.Sleep(AttackSpeed);
                                        AutoItX.Send("{R DOWN}");
                                        Thread.Sleep(50);
                                        AutoItX.Send("{R UP}");
                                        Thread.Sleep(AttackSpeed);
                                    }
                                }
                                if (AttackMode == 2)
                                {
                                    AutoItX.Send("{Z Down}");
                                    Thread.Sleep(50);
                                    AutoItX.Send("{Z UP}");
                                    string EnemyList = Properties.Settings.Default.EnemyList;
                                    string[] _Enemy = EnemyList.Split(',');
                                    foreach (var item in _Enemy)
                                    {
                                        if(item.Length > 2)
                                        {
                                            if (item == Memory.readString($"KnightOnLine.exe+0x00AFD39C, 0x1FC, 0x1CC, 0x134, 0x8, 0x0"))
                                            {
                                                AutoItX.Send("{1 DOWN}");
                                                Thread.Sleep(50);
                                                AutoItX.Send("{1 UP}");
                                                Thread.Sleep(AttackSpeed);

                                            }
                                        }

                                    }

                                }
                                if (AttackMode == 3)
                                {
                                    //Memory.writeMemory("KnightOnLine.exe+00B052BC, 2E0", "int", "912157304");
                                    //Thread.Sleep(50);
                                    //Memory.writeMemory("KnightOnLine.exe+00B052BC, 2E4", "int", "28");
                                    //Thread.Sleep(50);
                                    //Memory.writeMemory("KnightOnLine.exe+00B052BC, 1084", "int", "107500");
                                    //Thread.Sleep(50);
                                    //Memory.writeMemory("KnightOnLine.exe+00B052BC, 430", "int", "6");
                                    //Thread.Sleep(50);
                                    //Thread.Sleep(5000);
                                    string EnemyList = Properties.Settings.Default.EnemyList;
                                    string[] _Enemy = EnemyList.Split(',');
                                    foreach (var __Enemy in _Enemy)
                                    {
                                        if (__Enemy.Length > 2)
                                        {
                                            Debug.WriteLine("Attacking: " + __Enemy);
                                            Memory.writeMemory("KnightOnLine.exe+00B052BC, 6CC", "int", $"{__Enemy}");
                                            Memory.writeMemory("KnightOnLine.exe+00B052BC, 1048", "int", "256");
                                            Thread.Sleep(500);
                                        }

                                        //ANIMATIONS
                                        //Memory.writeMemory("KnightOnLine.exe+00B052BC, 2E0", "int", "912161984");
                                        //Memory.writeMemory("KnightOnLine.exe+00B052BC, 2E4", "int", "93");
                                        //SELECT ENEMY
                                        //Memory.writeMemory("KnightOnLine.exe+00B052BC, 6CC", "int", $"{enemy}");
                                        //ATTACK THE ENEMY
                                        ////Debug.WriteLine("Writing Memory: " + enemy);
                                        ////Thread.Sleep(500);

                                        //Thread.Sleep(500);
                                        //Memory.writeMemory("KnightOnLine.exe+00B052BC, 2E0", "int", "912157304");
                                        ////Thread.Sleep(500);
                                        ////Memory.writeMemory("KnightOnLine.exe+00B052BC, 2E4", "int", "28");
                                        ////Thread.Sleep(500);
                                        ////Memory.writeMemory("KnightOnLine.exe+00B052BC, 430", "int", "6");
                                        ////Thread.Sleep(500);

                                    }
                                }
                            }
                        }

                    }
                }

            };
            AutoAttackWorker.CancelAsync();
            AutoAttackWorker.RunWorkerAsync();
        }

        public static void AutoBuffer()
        {
            AutoBufferWorker = new BackgroundWorker();
            AutoBufferWorker.WorkerSupportsCancellation = true;
            AutoBufferWorker.DoWork += AutoBufferWork;
            void AutoBufferWork(object sender, DoWorkEventArgs e)
            {
                Memory.OpenProcess(gameProcId);
                while (Properties.Settings.Default.IsRunning)
                {
                    if (AutoBufferWorker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (gameProcId > 0)
                    {

                        if (Properties.Settings.Default.IsACActive)
                        {

                            int DEFENCE_STAT = Memory.read2Byte($"KnightOnLine.exe+{Offsets.KnightOnLine}, {Offsets.CHR_STAT_DEFENCE}");
                            if (DEFENCE_STAT < 450)
                            {
                                Properties.Settings.Default.IsBuffing = true;
                                AutoItX.Send("{Z DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{Z UP}");
                                AutoItX.Send("{7 DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{7 UP}");
                                Thread.Sleep(2500);
                                AutoItX.Send("{X DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{X UP}");
                                AutoItX.Send("{7 DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{7 UP}");
                                AutoItX.Send("{Z DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{Z UP}");
                                Properties.Settings.Default.IsBuffing = false;
                            }

                        }
                        if (Properties.Settings.Default.IsHPBuffActive)
                        {
                            var HP_STAT = Memory.read2Byte($"KnightOnLine.exe+{Offsets.KnightOnLine}, {Offsets.CHR_MAX_HP}");
                            if (HP_STAT < 2500)
                            {
                                Properties.Settings.Default.IsBuffing = true;
                                AutoItX.Send("{8 DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{8 UP}");
                                Properties.Settings.Default.IsBuffing = false;
                            }

                        }

                    }
                    Thread.Sleep(50);
                }
            };
            AutoBufferWorker.CancelAsync();
            AutoBufferWorker.RunWorkerAsync();
        }

        public static void AutoPrayerWildness()
        {
            AutoPrayerWildnessWorker = new BackgroundWorker();
            AutoPrayerWildnessWorker.WorkerSupportsCancellation = true;
            AutoPrayerWildnessWorker.DoWork += AutoPrayerWildnessWork;
            void AutoPrayerWildnessWork(object sender, DoWorkEventArgs e)
            {
                Memory.OpenProcess(gameProcId);
                while (Properties.Settings.Default.IsRunning)
                {
                    if (AutoPrayerWildnessWorker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        return;
                    }
                    if (gameProcId > 0)
                    {
                        if (Properties.Settings.Default.IsBuffing == false)
                        {
                            if (Properties.Settings.Default.IsPrayerofGodsPowerActive)
                            {
                                AutoItX.Send("{3 DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{3 UP}");


                            }
                            if (Properties.Settings.Default.IsWildnessActive)
                            {
                                AutoItX.Send("{4 DOWN}");
                                Thread.Sleep(50);
                                AutoItX.Send("{4 UP}");
                            }

                            Thread.Sleep(20000);
                        }
                    }
                    Thread.Sleep(50);
                }
            };
            AutoPrayerWildnessWorker.CancelAsync();
            AutoPrayerWildnessWorker.RunWorkerAsync();
        }
    }
}
